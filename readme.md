SideFX Houdini 12.x HDK Visual Studio Startup Project

Big thanks to mantragora @ http://www.sidefx.com/index.php?option=com_forum&Itemid=172&page=viewtopic&t=25179

.dll and .dso are to be writen to My Documents\houdini12.5\dso

Requirements:

- Houdini 12.x with the HDK installed

- A few System variables:

	VS2008_PATH - points to "C:\Program Files (x86)\Microsoft Visual Studio 9.0"
	H12_VERSION - sets Houdini version build I want to compile for. For example "12.0.552" 
	H12_PATH - points to "C:\Program Files\Side Effects Software\Houdini %H12_VERSION%" 


- For the VS2012 package you need:
	Visual Studio 2008 (We need v90)
	Visual Studio 2008 x64 Compilers and Tools (Be sure to select them in the installation)
	Visual Studio 2010 (We need this to let VS2012 talk to v90)
	Visual Studio 2010 x64 Compilers and Tools (Be sure to select them in the installation)
	Visual Studio 2012